import pickle
from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory
import shopyourway.controllers.home_controller as home
import shopyourway.controllers.account_controller as account
import shopyourway.controllers.store_controller as store
import shopyourway.controllers.products_controller as product
import os

# config.add_route('store', '/buy/{name_fragment}')

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.set_session_factory(SignedCookieSessionFactory('qw32hGDT209N7yRFD'))
    init_includes(config)

    init_routing(config)

    config.scan()
    zip_file = "{}{}".format(os.path.abspath(os.path.dirname(__file__)), "/data/zip2latlon.p")
    zip2LatLon = pickle.load(open( zip_file, "rb"))
    store.StoreController.zip2LatLon = zip2LatLon

    return config.make_wsgi_app()


def init_includes(config):
    config.include('pyramid_jinja2')
    config.include('pyramid_chameleon')
    config.include('pyramid_handlers')


def add_controller_routes(config, ctrl, prefix):
    config.add_handler(prefix + 'ctrl_index', '/' + prefix, handler=ctrl, action='index')
    config.add_handler(prefix + 'ctrl_index/', '/' + prefix + '/', handler=ctrl, action='index')
    config.add_handler(prefix + 'ctrl', '/' + prefix + '/{action}', handler=ctrl)
    config.add_handler(prefix + 'ctrl/', '/' + prefix + '/{action}/', handler=ctrl)
    config.add_handler(prefix + 'ctrl_id', '/' + prefix + '/{action}/{id}', handler=ctrl)


def init_routing(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_handler('root',
                       '/',
                       handler=home.HomeController, action='index')
    add_controller_routes(config, home.HomeController, 'home')
    add_controller_routes(config, account.AccountController, 'account')
    config.add_handler('stores_ctrl_index', '/stores', handler=store.StoreController, action='index')
    config.add_handler('stores_ctrl_index/', '/stores/', handler=store.StoreController, action='index')
    config.add_handler('products_ctrl_index', '/products', handler=product.ProductController, action='index')
    config.add_handler('products_ctrl_index/', '/products/', handler=product.ProductController, action='index')