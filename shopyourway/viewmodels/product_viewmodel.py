from shopyourway.viewmodels.viewmodelbase import ViewModelBase


class ProductViewModel(ViewModelBase):
    def __init__(self):
        pass

    def from_dict(self, data_dict):
        self.__dict__.update(data_dict)

    def from_json(self, data_json):

        print("{}".format(data_json))
