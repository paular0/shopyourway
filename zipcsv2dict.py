import pickle

zip2LatLon = {}

with open('ziplatlon.csv', 'r') as f:
    line = f.readline()
    while line:
        print(line)
        parts = line.split(',')
        key,values = parts[0],parts[1:]
        zip2LatLon[key] = values
        line = f.readline()

print("{}".format(zip2LatLon))
print(zip2LatLon['60010'])

f = open('zip2latlon.p', 'wb')
pickle.dump(zip2LatLon, f)
f.close()